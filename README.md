* Rating system (20H)
    * fields (2H)
        * value
    * parent relations (2H)
        * polymorphs to other (2H)
    * children relations (16H)
        * comments (5H)
            * Relations (3H)
            * fields (2H)
                * user
                * description
                * rating
                * status (active, banned)
        * likes (from users) (5H)
        * comment_claims (6H)
            * Relations (1H)
            * Rules (3H)
            * fields (2H)
                * comment
                * user
                * description
		        * status (recently_created, accepted, declined)

* Films (18H + 84DH)
    * fields (2H)
        * title (ML)
        * original_title
        * premier_date
    * parent relations (10H)
        * countries
        * personas
        * genres
        * reviews
        * desired_films
        * playlists
        * Rating system
        * tags
    * children relations (6H + 84DH)
        * simular_films (2H)
        * third-party sources (84H) (difficult coding, X25$)
            * fields (2H)
                * rating
                * url
            * services (82H)
                * IMDB (16H)
                * KP (24H)
                * IVI (20H)
                * MEGOGO (22H) 
        * Filesystem (4H)
            * preview (ML)
            * ML
            * photos
            * videos
            * youtube links

* Dictionaries (ML everything) (22H)
    * genres (4H)
    * tags (4H)
    * GEO (14H)
        * countries  (4H)
        * regions (5H)
        * cities (5H)

* Persons (30H)
    * fields (6H)
        * full name (ML)
        * Original name
        * Country
        * Region
        * City
        * Birthday
        * Files
            * Preview
            * Galery
    * parent relations (14H)
        * Films (by roles) (3H)
        * News (by personas) (4H)
        * Rating system (7H)
    * children relations (10H)
        * Preson roles (6H)
            * title (ML)   
        * Person facts (4H)
            * title (ML)

* Reviews (16H)
    * Fields (5H)
        * title
        * description
        * File
            * Preview
    * Parent relations (5H)
        * Film (1H)
        * Rating system (5H)
    * Children relations (4H)
        * content_block (4H)
            * description
            * File (photo or video)
            * file description

* Articles (14H)
    * Fields (4H)
        * title (ML)
        * description (ML)
        * Files
            * preview
    * Parent relations (10H)
        * Films (1H)
        * Tags (2H)
        * Personas (3H)
        * Rating system (6H)

* Playlists (12H)
    * Fields (4H)
        * title (ML)
        * description (ML)
    * Parent relations (8H)
        * Films (4H)
        * Rating system (4H)

* Users (44H)
    * Passport (6H)
    * Auth (8H)
    * Fields (8H)
        * first name
        * last name
        * email
        * phone
        * birthday
        * sex
        * about
        * status (activated, banned, disactivated)
        * last action at (datetime)
        * Files
            * Preview
            * Photos
    * Parent Relations (4H)
        * Subscribers (users)
        * Playlists
        * Dialogs
    * Children Relations (18H)
        * Desider films (4H)
        * Roles (14H)

* Dialogs (30H)
    * fields (2H)
        * status
    * Parent relations (2H)
        * Users
    * Children Relations (26H)
        * Messages (14H)
        * Votings (12H)
    
