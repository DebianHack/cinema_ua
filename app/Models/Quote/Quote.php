<?php

namespace App\Models\Quote;

use Egretos\RestModel\Model;

/**
 * Class Quote
 * @package App\Models\Quote
 *
 * @property string $content
 * @property string $author
 */
class Quote extends Model
{
    public $connection = 'quotes';
    protected $resource = 'random';
}
