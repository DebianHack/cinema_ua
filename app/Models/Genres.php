<?php


namespace App\Models;

use App\Models\Translations\GenresTranslations;
use App\Traits\Uuid;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

//I used the php artisan ide-helper: models command but the models are not highlighted.
//Tell me what actions need to be done to make them appear
/**
 * Class Genres
 * @package App\Models
 */
//
class Genres extends Model
{
    use Uuid, Translatable;
// Removing the index increment
    public $incrementing = false;
// Defining the index as a closed variable
    protected $fillable = ['id'];
// Initializing the class composition
    public $translatedAttributes = ['title', 'description', 'slug'];
}
