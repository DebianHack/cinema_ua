<?php


namespace App\Models\Translations;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

/**
 * Class GenresTranslations
 * @package App\Models\Translations
 */
class GenresTranslations extends Model
{
    use Uuid;
    // Removing the index increment
    public $incrementing = false;
    // Removing the creation and update times (
    // I write that there is an error({Model} and HasTimestamps define the same
    // property timestamps)They say that this is a mistake of PHPstorm itself)
    public $timestamps = false;
    // Variables whose values need to be translated
    protected $fillable = [
        'title',
        'description',
        'slug'
    ];
}
