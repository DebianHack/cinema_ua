<?php

namespace App\Traits;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * Trait Uuid
 * @package App\Traits
 *
 * @mixin Eloquent
 */
trait Uuid
{
    protected static function boot()
    {
        parent::boot();
        static::creating(function ($post) {
            $post->{$post->getKeyName()} = (string) Str::uuid();
        });
    }
    public function getIncrementing()
    {
        return false;
    }
    public function getKeyType()
    {
        return 'string';
    }
}
