<?php

/** @noinspection PhpUnused */

namespace App\Http\Controllers;

use App\Models\Quote\Quote;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Method to check an Application is works good
     *
     * @return JsonResponse
     */
    public function healthCheck(): JsonResponse
    {
        /** @var Quote $quote */
        $quote = Quote::find('');

        return response()->json([
            'status' => 'OK',
            'message' => $quote->content,
            'author' => $quote->author,
        ]);
    }
}
