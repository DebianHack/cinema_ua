<?php
namespace App\Http\Controllers;

use App\Models\Genres;
use App\Http\Resource\GenresResource;


class GenresController extends Controller
{
    /**
     * Class CategoryController
     * @package App\Http\Controllers
     * @noinspection PhpUnused
     */
    public function index() {
        return GenresResource::collection(
            Genres::withTranslation()->paginate('per_page', 15)
        );
    }
    /**
     * @param Genres $genres
     * @return GenresResource
     */
    public function show(Genres $genres) {

        return GenresResource::make( $genres->load(['translations']) );
    }
}
