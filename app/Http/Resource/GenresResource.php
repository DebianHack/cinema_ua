<?php

namespace App\Http\Resource;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\Genres;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Concerns\HasRelationships;


class GenresResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request $request
     * @return array
     */
    //Did I also need to use not array_merge? It's just that I'm not yet our examples of use that you suggested
    public function toArray($request)
    {
        if ($this->relationLoaded('translations') && $this->translation) {
            $translations = array_merge([
                'title' => $this->translation->title,
                'description' => $this->translation->description,
                'slug' => $this->translation->slug,
            ], $this->getTranslationsArray());
        } else {
            $translations = [];
        }
        return ['id' => $this->id] + [$translations];
//
    }
}

