<?php
/**
 * Configuration of REST`ful resources
 */
return [
    // Connection which will be used for models by default
    'default_connection' => env('REST_CONNECTION', 'quotes'),

    'connections' => [
        'quotes' => [
            'domain' => 'https://staging.quotable.io',
        ],
    ],
];
