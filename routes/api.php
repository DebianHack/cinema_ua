<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;



Route::apiResource('genres', 'GenresController')->only([
    'index', 'show',
]);

